﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System.Threading.Tasks;
using IdentityModel.Client;
using NUnit.Framework;
using TemplateApi.Testing.Tests;
using TemplateBase.IntegrationTests.Bases;

namespace TemplateApi.Testing
{
    [TestFixture]
    public class TemplateApiBaseTest : IntegrationTest<IdentityControllerTests, Startup>
    {
        [OneTimeSetUp]
        public async Task SetupFixture()
        {
            await ResetDatabase();
            var accessToken = await GetAccessToken("admin", "ASDFasdf!");
            ApiClient.SetBearerToken(accessToken);
        }

        [OneTimeTearDown]
        public async Task TeardownFixture()
        {
            await DeleteDatabase();
        }

        protected string IdentityUrl = "/v1.0/identity";
    }
}