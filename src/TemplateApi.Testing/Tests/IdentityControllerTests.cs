﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System.Threading.Tasks;
using NUnit.Framework;

namespace TemplateApi.Testing.Tests
{
    [TestFixture]
    public class IdentityControllerTests : TemplateApiBaseTest
    {
        [TestFixture]
        public class TheGetIdentityMethod : IdentityControllerTests
        {
            [Test]
            public async Task HappyPath()
            {
                var getJourneyResponse = await ApiClient.GetAsync(IdentityUrl);

                var getJourneyResult = await getJourneyResponse.Content.ReadAsStringAsync();

                Assert.IsNotNull(getJourneyResult);
            }
        }
    }
}