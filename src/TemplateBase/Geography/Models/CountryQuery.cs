﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

namespace TemplateBase.Geography.Models
{
    public class CountryQuery
    {
        public bool? Enabled { get; set; }
    }
}