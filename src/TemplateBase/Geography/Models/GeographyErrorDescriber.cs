﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using TemplateBase.Common.Models;

namespace TemplateBase.Geography.Models
{
    public class GeographyErrorDescriber
    {
        public virtual Error EnableCountryError()
        {
            return new Error
            {
                Code = nameof(EnableCountryError),
                Description = "Unable to enable country"
            };
        }

        public virtual Error CountryAlreadyEnabled()
        {
            return new Error
            {
                Code = nameof(CountryAlreadyEnabled),
                Description = "country already enabled"
            };
        }
    }
}