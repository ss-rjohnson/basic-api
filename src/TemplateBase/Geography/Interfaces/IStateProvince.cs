﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

namespace TemplateBase.Geography.Interfaces
{
    public interface IStateProvince
    {
        string Name { get; set; }
        string Abbrev { get; set; }
        string Code { get; set; }
    }
}