﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System.Collections.Generic;
using System.Threading.Tasks;
using TemplateBase.Geography.Models;

namespace TemplateBase.Geography.Interfaces
{
    public interface IStateProvinceStore
    {
        Task<List<T>> GetStateProvincesForCountry<T>(string id) where T : StateProvinceDto;
    }
}