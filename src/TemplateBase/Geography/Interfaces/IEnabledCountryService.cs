﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System.Linq;
using System.Threading.Tasks;
using TemplateBase.Common.Models;
using TemplateBase.Common.Services.Interfaces;
using TemplateBase.Geography.Entities;

namespace TemplateBase.Geography.Interfaces
{
    public interface IEnabledCountryService : IService<EnabledCountry>
    {
        IQueryable<EnabledCountry> EnabledCountries { get; }

        Task<Result> EnableCountry(string iso2);
    }
}