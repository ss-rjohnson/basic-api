﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using AutoMapper;
using TemplateBase.Users.Entities;
using TemplateBase.Users.Models;

namespace TemplateBase.Users.Projections
{
    public class UserProjections : Profile
    {
        public UserProjections()
        {
            CreateMap<User, UserDto>()
                .IncludeAllDerived();
        }
    }
}