﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

namespace TemplateBase.Users.Interfaces
{
    public interface IRole
    {
        string Name { get; set; }
        int Id { get; set; }
    }
}