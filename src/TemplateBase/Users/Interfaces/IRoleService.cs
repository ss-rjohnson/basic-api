﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using Microsoft.AspNetCore.Identity;
using TemplateBase.Common.Services.Interfaces;
using TemplateBase.Users.Entities;

namespace TemplateBase.Users.Interfaces
{
    public interface IRoleService : IService<Role>,
        IRoleStore<Role>,
        IQueryableRoleStore<Role>,
        IRoleClaimStore<Role>
    {
    }
}