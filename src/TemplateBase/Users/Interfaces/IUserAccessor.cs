﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System.Security.Claims;
using System.Threading.Tasks;

namespace TemplateBase.Users.Interfaces
{
    public interface IUserAccessor
    {
        Task<IUser> GetUser(ClaimsPrincipal principal);
    }
}