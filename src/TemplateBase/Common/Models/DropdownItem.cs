﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

namespace TemplateBase.Common.Models
{
    public class DropdownItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}