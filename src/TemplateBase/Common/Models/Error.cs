﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

namespace TemplateBase.Common.Models
{
    public class Error
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}