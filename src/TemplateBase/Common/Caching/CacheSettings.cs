﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System;
using System.Diagnostics.CodeAnalysis;

namespace TemplateBase.Common.Caching
{
    [ExcludeFromCodeCoverage]
    public class CacheSettings
    {
        public TimeSpan? DefaultExpiration { get; set; }
    }
}