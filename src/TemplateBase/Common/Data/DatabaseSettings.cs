﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System.Diagnostics.CodeAnalysis;

namespace TemplateBase.Common.Data
{
    [ExcludeFromCodeCoverage]
    public class DatabaseSettings
    {
        public int Timeout { get; set; }
        public string ConnectionStringName { get; set; }
    }
}