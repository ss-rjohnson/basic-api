﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using TemplateBase.Common.Data.Enums;

namespace TemplateBase.Common.Data.Interfaces
{
    public interface IObjectState
    {
        public ObjectState ObjectState { get; set; }
    }
}