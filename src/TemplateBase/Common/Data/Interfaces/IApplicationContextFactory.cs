﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using Microsoft.EntityFrameworkCore.Design;
using TemplateBase.Common.Data.Contexts;

namespace TemplateBase.Common.Data.Interfaces
{
    public interface IApplicationContextFactory : IDesignTimeDbContextFactory<ApplicationContext>
    {
    }
}