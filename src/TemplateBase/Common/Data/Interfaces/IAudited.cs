﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

namespace TemplateBase.Common.Data.Interfaces
{
    public interface IAudited :
        ICreationAudited,
        IModificationAudited
    {
    }
}