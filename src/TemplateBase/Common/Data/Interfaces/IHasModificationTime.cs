﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System;

namespace TemplateBase.Common.Data.Interfaces
{
    public interface IHasModificationTime
    {
        /// <summary>The last modified time for this entity.</summary>
        DateTime? Updated { get; set; }
    }
}