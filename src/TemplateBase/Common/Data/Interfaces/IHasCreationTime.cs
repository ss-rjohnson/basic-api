﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System;

namespace TemplateBase.Common.Data.Interfaces
{
    public interface IHasCreationTime
    {
        /// <summary>Creation time of this entity.</summary>
        DateTime Created { get; set; }
    }
}