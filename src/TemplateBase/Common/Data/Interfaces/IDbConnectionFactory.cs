﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using System.Data;

namespace TemplateBase.Common.Data.Interfaces
{
    public interface IDbConnectionFactory
    {
        IDbConnection DbConnection { get; }
    }
}