﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using TemplateBase.Users.Entities;

namespace TemplateBase.Common.Data.Interfaces
{
    public interface IUserModificationAudited : IModificationAudited
    {
        /// <summary>Reference to the last modifier user of this entity.</summary>
        User LastModifierUser { get; set; }
    }
}