﻿#region Header

// /*
// Copyright (c) 2021 SolutionStream. All rights reserved.
// Author: Rod Johnson, Architect, Solution Stream
// */

#endregion

using AutoMapper;
using TemplateBase.Common.Data.Interfaces;

namespace TemplateBase.Common.Services.Interfaces
{
    public interface IService<TEntity> where TEntity : class, IObjectState
    {
        public MapperConfiguration ProjectionMapping { get; }
        public IRepositoryAsync<TEntity> Repository { get; }
    }
}